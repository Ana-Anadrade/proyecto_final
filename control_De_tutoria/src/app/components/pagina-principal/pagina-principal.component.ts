import { Component } from '@angular/core';
import {FormGroup,FormControl,FormBuilder,Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { UserserviceService } from 'src/app/servivices/userservice.service'
@Component({
  selector: 'app-pagina-principal',
  templateUrl: './pagina-principal.component.html',
  styleUrls: ['./pagina-principal.component.css']
})
export class PaginaPrincipalComponent {
  constructor( 
    private User:UserserviceService,
    private ruta:Router 
    ){}
    onclick (
    ){
      this.User.logout()
      .then(()=> {
        this.ruta.navigate (["/Inicio"])
      })
      .catch(error=>console.log (error))
      
  }

}
