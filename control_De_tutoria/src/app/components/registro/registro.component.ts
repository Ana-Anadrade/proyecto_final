import { Component, OnInit } from '@angular/core';
import {FormGroup,FormControl,FormBuilder,Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { UserserviceService } from 'src/app/servivices/userservice.service'
import { UsuarioService } from 'src/app/servivices/usuario.service';
@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent {
  FormRegister:FormGroup;
  constructor(    
    private Usuario:UsuarioService,
    private User:UserserviceService ,   
    private ruta:Router
    )
      {
        this.FormRegister=new FormGroup(
          {
            email: new FormControl(),
            password:new FormControl(),
            nombre: new FormControl(),
            cedula: new FormControl()

          }
        );
  }
 
  async Register(){
    this.User.registre(this.FormRegister.value)
    .then(response =>
      {this.ruta.navigate (["/Inicio"])}
      )
      .catch(error=>console.log (error))
  }
  async Registerbase(){
    console.log(this.FormRegister.value)
    const response =await this.Usuario.addPLace(this.FormRegister.value);
    console.log(response);
  }
  
  async OnSubmit(){
    this.Registerbase();
    this.Register();

  }
}
