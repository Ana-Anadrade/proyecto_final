import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
///rutas 
import { app_routing } from './app.routes';
///////////////////////////
//componente
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PaginaPrincipalComponent } from './components/pagina-principal/pagina-principal.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { RegistroComponent } from './components/registro/registro.component';
import { RecuperarComponent } from './components/recuperar/recuperar.component';
import { AgendaComponent } from './components/agenda/agenda.component';
import { PerfilComponent } from './components/perfil/perfil.component';
import { CambioComponent } from './components/cambio/cambio.component';
import { ListadoComponent } from './components/listado/listado.component';
import { initializeApp,provideFirebaseApp } from '@angular/fire/app';
import { environment } from '../environments/environment';
import { provideAuth,getAuth } from '@angular/fire/auth';
import { provideFirestore,getFirestore } from '@angular/fire/firestore';
////////////////
//forms
import { FormsModule,NgForm,NgModel} from '@angular/forms';
import { ReactiveFormsModule} from '@angular/forms'
///////////////////////////////////////////////
@NgModule({
  declarations: [
    AppComponent,
    PaginaPrincipalComponent,
    InicioComponent,
    RegistroComponent,
    RecuperarComponent,
    AgendaComponent,
    PerfilComponent,
    CambioComponent,
    ListadoComponent
  ],
  imports: [
    //formm
    FormsModule,
    ReactiveFormsModule,
    /////
    BrowserModule,
    app_routing,
    AppRoutingModule,
    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideFirestore(()=> getFirestore()),
    provideAuth(() => getAuth()),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
