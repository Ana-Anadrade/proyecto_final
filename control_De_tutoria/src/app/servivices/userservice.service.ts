import { Injectable } from '@angular/core';
import { Auth, createUserWithEmailAndPassword,signInWithEmailAndPassword, signOut} from '@angular/fire/auth';
@Injectable({
  providedIn: 'root'
})
export class UserserviceService {

  constructor(private auth:Auth) { }
  registre({email, password}:any){
    return createUserWithEmailAndPassword(this.auth, email, password)
  }
  incio({email, password}:any){
    return signInWithEmailAndPassword(this.auth, email, password)
  }
  logout(){
    return signOut(this.auth)
  }
}
