export default interface Usuario{
    id?: string;
    email:string;
    password: string;
    nombre: string;
    cedula: number;
}