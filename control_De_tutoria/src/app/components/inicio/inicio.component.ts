import { Component } from '@angular/core';
import {FormGroup,FormControl,FormBuilder,Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { UserserviceService } from 'src/app/servivices/userservice.service'
@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent {
  Forminicio:FormGroup;
  constructor(    
    private User:UserserviceService ,   
    private ruta:Router
    )
      {
        this.Forminicio=new FormGroup(
          {
            email: new FormControl,
            password:new FormControl
          }
        );
  }
  onSubmit(){
    this.User.incio(this.Forminicio.value)
    .then(response =>
      {this.ruta.navigate (["/Pagprincial"])}
      )
      .catch(error=>console.log (error))
  } 

}
