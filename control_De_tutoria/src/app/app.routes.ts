import { Component } from "@angular/core";
import { RouterModule, Routes } from "@angular/router"
import { canActivate,redirectUnauthorizedTo } from '@angular/fire/auth-guard';
import { 
    PaginaPrincipalComponent,
    InicioComponent,
    RegistroComponent,
    RecuperarComponent,
    AgendaComponent,
    PerfilComponent,
    CambioComponent,
    ListadoComponent,

} from "./components/index.pagina";


const app_routes: Routes= [ 
    {path:'Pagprincial',component:PaginaPrincipalComponent,
    ...canActivate(()=>redirectUnauthorizedTo(['/Inicio']))
    },
    {path:'Agenda',component:AgendaComponent,
    ...canActivate(()=>redirectUnauthorizedTo(['/Inicio']))
    },
    {path:'Asistencia',component:ListadoComponent,
    ...canActivate(()=>redirectUnauthorizedTo(['/Inicio']))
    },
    {path:'Cambio',component:CambioComponent,
    ...canActivate(()=>redirectUnauthorizedTo(['/Inicio']))
    },
    {path:'Perfil',component:PerfilComponent,
    ...canActivate(()=>redirectUnauthorizedTo(['/Inicio']))
    },
    {path:'Inicio',component:InicioComponent},
    {path:'Registro',component:RegistroComponent},
    {path:'Recuperar',component:RecuperarComponent},
    {path:'**',pathMatch:'full',redirectTo:'/Inicio'}
];
export const app_routing = RouterModule.forRoot(app_routes,{useHash:true});