import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserserviceService } from 'src/app/servivices/userservice.service'
@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent {
  constructor( 
    private User:UserserviceService,
    private ruta:Router 
    ){}
    onclick (
    ){
      this.User.logout()
      .then(()=> {
        this.ruta.navigate (["/Inicio"])
      })
      .catch(error=>console.log (error))
      
  }

}
