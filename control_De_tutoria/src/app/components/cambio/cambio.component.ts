import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserserviceService } from 'src/app/servivices/userservice.service'
@Component({
  selector: 'app-cambio',
  templateUrl: './cambio.component.html',
  styleUrls: ['./cambio.component.css']
})
export class CambioComponent {
  constructor( 
    private User:UserserviceService,
    private ruta:Router 
    ){}
    onclick (
    ){
      this.User.logout()
      .then(()=> {
        this.ruta.navigate (["/Inicio"])
      })
      .catch(error=>console.log (error))
      
  }
}
