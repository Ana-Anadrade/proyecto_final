import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserserviceService } from 'src/app/servivices/userservice.service'
@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent {
  constructor( 
    private User:UserserviceService,
    private ruta:Router 
    ){}
    onclick (
    ){
      this.User.logout()
      .then(()=> {
        this.ruta.navigate (["/Inicio"])
      })
      .catch(error=>console.log (error))
      
  }
}
