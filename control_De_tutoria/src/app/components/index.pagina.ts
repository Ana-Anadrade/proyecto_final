export { PaginaPrincipalComponent } from './pagina-principal/pagina-principal.component';
export { InicioComponent } from './inicio/inicio.component';
export { RegistroComponent } from './registro/registro.component';
export { RecuperarComponent } from './recuperar/recuperar.component';
export{ AgendaComponent } from './agenda/agenda.component';
export{ PerfilComponent } from './perfil/perfil.component';
export { CambioComponent } from './cambio/cambio.component';
export{ ListadoComponent } from './listado/listado.component';